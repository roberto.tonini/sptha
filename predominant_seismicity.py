#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import numpy as np
from shapely.geometry import Point,Polygon,MultiPolygon


def parsing_mesh_cubit(filename_mesh, filename_attributes, nv):
    """
    Input:  
      - filename: CUBIT mesh file
      - nv: number of vertices in each mesh element
    Output:
      - n_el: number of subfaults
      - x_el, y_el, z_el: coordinates of each subfault's verts
      - s_el, d_el, r_el: strike, dip, rake of each subfault's verts
      - verts: array of verts 
      - w_depth: water depth of each element
    """

  
    #rake = '90.0'   # manual setting of rake
    coords = []
    angles = []
    depth = []
    with open(filename_mesh, "r") as f:
        ic = 0
        for line in f:
            if (ic < 9):
                pass
            else:
                if line.startswith("**"):
                    break
                else:
                    tmp = line.strip().split(",")
                    inode = int(tmp[0])
                    coords.append([float(tmp[i]) for i in [1,2,3]])
      
            ic += 1
    
    newstart = ic + 3
    
    elements = []
    with open(filename_mesh, "r") as f:
        ic = 0
        for line in f:
            if (ic < newstart):
                pass
            else:
                if line.startswith("**"):
                    break
                else:
                    tmp = line.strip().split(",")
                    inode = int(tmp[0])
                    elements.append([int(tmp[i]) for i in range(1,nv+1)])
      
            ic += 1
  
    with open(filename_attributes, "r") as f:
        f.readline()
        for line in f.readlines():
            tmp = line.strip().split()
            #tmp.append(rake)
            angles.append([float(tmp[i]) for i in [3,4,5]])
            depth.append([float(tmp[i]) for i in [6]])
             

    A = np.array(elements)
    B = np.array(coords)
    C = np.array(angles)
    D = np.array(depth)
  
    n_el, tmp = np.shape(A[:,1:])
    
    x_el = np.zeros((n_el,nv))
    y_el = np.zeros((n_el,nv))
    z_el = np.zeros((n_el,nv))
    
    s_el = np.zeros((n_el,nv))
    d_el = np.zeros((n_el,nv))
    r_el = np.zeros((n_el,nv))
  
    w_depth = np.zeros((n_el,nv))
  
    verts = np.zeros((n_el,nv+1,2))
    for ip in range(n_el):
        ind = A[ip,:]
        for iv in range(nv+1):
            if (iv == nv):
                verts[ip,iv,:] = B[ind[0]-1,:-1]
            else:
                verts[ip,iv,:] = B[ind[iv]-1,:-1]
                x_el[ip,iv], y_el[ip,iv], z_el[ip,iv] = B[ind[iv]-1,:]
                d_el[ip,iv], s_el[ip,iv], r_el[ip,iv] = C[ind[iv]-1,:]
                w_depth[ip,iv] = D[ind[iv]-1,:]
  
    return n_el, x_el, y_el, z_el, s_el, d_el, r_el, verts, w_depth


def create_seismogenetic_boundary(filename):
    """
    Define seismogenetic boundary
    b = array of boundary points formatted as [ [x1,y1], [x2,y2], ..., [xn,yn] ]
    """
  
    A = np.loadtxt(filename, skiprows=1)
    xb = A[:,0]
    yb = A[:,1]
    #zb = A[:,2]
    b = A[:,0:2]
    #return xb, yb, zb, b
    return xb, yb, b
 

def calc_mesh_barycenters(xp, nv):
    """
    Calculate baricenters of each subfault
    """
    xc = np.sum(xp, axis=1)/nv 
    return xc


def mesh_selection_optimize(idp, pFault, verts, areaFault, areaSF):
    """
    """
    areaSelSF = sum(areaSF)
    if (len(idp) > 1):      
        diff1 = 1
        tmp1 = idp
        tmp2 = areaSF
        bpsID, bpsArea = mesh_selection_sort(idp, pFault, verts, 
                                   areaSF, areaSelSF, 0.75)
   
        nbps = len(bpsID)
        for iopt in range(nbps): 
            ratioTot = areaSelSF/areaFault
            if (ratioTot > 1.0):
                #print("    ratio maggiore {0}".format(ratioTot))
                diff1 = ratioTot-1.0
                tmp1 = idp
                tmp2 = areaSF
                areaSelSF = sum(tmp2)-bpsArea[iopt]
                idp.remove(bpsID[iopt])
                areaSF.remove(bpsArea[iopt])
        
                ratioTmp = areaSelSF/areaFault
                if (ratioTmp < 1.0):
                    #print("    ratio minore {0}".format(ratioTmp))
                    diff2 = 1.0-ratioTmp
                    if (diff2 > diff1):
                        idp = tmp1
                        areaSF = tmp2
                    break
          
            else:
                pass
  
    #return idp, areaSelSF
    return idp, areaSF


def mesh_selection_sort(idp, pFault, verts, areaSF, areaSelSF, inSF):
    """
    """
    boundary = []
    boundaryID = []
    boundaryArea = []
  
    for ips in range(len(idp)):
        pSF = Polygon(verts[idp[ips]])
        if ( pFault.contains(pSF) ):
            pass
        else:
            pInter = pFault.intersection(pSF)
            ratio = pInter.area/pSF.area
            if (ratio < inSF):
                tmp = areaSelSF-areaSF[ips]
                boundary.append(ratio)
                boundaryID.append(idp[ips])
                boundaryArea.append(areaSF[ips])

    # sorting from less to more overlapped patch
    idpBound = [x for (y,x) in sorted(zip(boundary,boundaryID))]
    areaBound = [x for (y,x) in sorted(zip(boundary,boundaryArea))]
  
    return idpBound, areaBound


def load_barycenters(filename):
    """
    """
    f = os.path.join(filename)
    if (os.path.exists(f)):
        tmp = np.loadtxt(f, skiprows=0)
        lon = tmp[:,0]
        lat = tmp[:,1]
        eq_lon = lon.tolist()
        eq_lat = lat.tolist()
        n = len(lon)
    else:
        msg = "ERROR: Barycenters file input is missing."
        sys.exit(msg)  

    return eq_lon, eq_lat, n


def main():
    """
    Running working example with Calabrian Arc (CA)
    """
    ps_dir = '/work/tonini/sptha/data/prevalent_seismicity'
    filename_mesh = os.path.join(ps_dir, 'CA_mesh_15km.inp')
    filename_attr = os.path.join(ps_dir, 'CA_attributes_wgs84_topo.txt')
    nv = 3
    
    return


if __name__ == "__main__":
    main()
