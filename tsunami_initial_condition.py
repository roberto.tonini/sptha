#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import subprocess
import numpy as np
from scipy.io import netcdf


def next_pow2(m):
    """
    Next power of 2 from m
    """
    n = 2
    while n < m: n = n * 2
    return n


def calc_kajiura_filter(z, h):
    """
    """

    dxmin = 1852.0                    # 1 arc-min in meters
    dx = 1.0                          # dx in arc-min
    fs = 1/(dx*dxmin)
    ny, nx = np.shape(z)

    NFFTx = 2^next_pow2(nx) 
    NFFTy = 2^next_pow2(ny)

    Y = np.fft.fft2(z, s=[NFFTx,NFFTy])
    fx = fs/2.*np.linspace(0,1,0.5*NFFTx)
    fy = fs/2.*np.linspace(0,1,0.5*NFFTy)
  
    f_twosidedx = np.hstack( (fx,fx[::-1]) )
    f_twosidedy = np.hstack( (fy,fy[::-1]) )
  
    xx = np.cosh(h*f_twosidedx*2.*np.pi)
    yy = np.cosh(h*f_twosidedy*2.*np.pi)
    denx, deny = np.meshgrid(xx, yy)
  
    kaj_filt = (Y/denx)/np.transpose(deny)
    p_filt = np.fft.ifft2(kaj_filt)
    zf = p_filt[:ny,:nx].real
  
    return zf


def calc_okada_external(working_dir):
    """
    Call an external fortran binary to calculate vertical dispalcement
    using Okada's formulas
    """

    process = os.path.join(working_dir, 'bin', './oka.x') 
    tmp_dir = os.path.join(working_dir, 'data')
    tmp = subprocess.check_output([process], shell=True, cwd=tmp_dir)
    
    nhead = 14
    lines = tmp.split(os.linesep)
    nok = len(lines)
    tmp = lines[nhead-1].split()
    nx = int(tmp[0])
    ny = int(tmp[1])
    nok = len(lines)-(nhead+1)
    xtmp = np.zeros((nok))
    ytmp = np.zeros((nok))
    ztmp = np.zeros((nok))
    
    for i in range(nok):
        tmp = lines[i+nhead].split()
        xtmp[i] = float(tmp[0])
        ytmp[i] = float(tmp[1])
        ztmp[i] = float(tmp[2])
    
    zok = np.reshape(ztmp, (ny,nx))
    xok = np.reshape(xtmp, (ny,nx))
    yok = np.reshape(ytmp, (ny,nx))
    return xok, yok, zok


def create_okada_external_input(working_dir, parameters):
    """
    Create the input to run the external fortran binary to calculate 
    vertical displacement with Okada's formulas (see calc_okada_external)
    """
    #header = 'LON_EPI	LAT_EPI	DEPTH_EPI(km)	L(km)	W(km)	STRIKE	DIP	RAKE	SLIP(m) LON_MIN LON_MAX LAT_MIN LAT_MAX KAJ_DEPTH(m)\n'
    #f = open(os.path.join(working_dir, 'data', 'param_oka.dat'), 'w')
    #f.write(header)
    #f.write(parameters)
    #f.close()
    tmp_dir = os.path.join(working_dir, 'data')
    cmd1 = os.path.join(working_dir, 'bin', './prep_FD.sh <<< ' + parameters)
    cmd2 = os.path.join(working_dir, 'bin', './prep_multiseg.sh <<< ' + parameters)
    subprocess.check_output([cmd1], shell=True, cwd=tmp_dir)                          
    subprocess.check_output([cmd2], shell=True, cwd=tmp_dir)
    return



def load_displacement_nc(filename, eqLon, eqLat):
    """
    Read a x,y,z netcdf file containing seafloor vertical displacement 
    """

    f = netcdf.netcdf_file(filename, 'r', mmap=False)
    tmp = f.variables.keys()
   
    #x = f.variables[tmp[1]][:] + eqLon
    #y = f.variables[tmp[0]][:] + eqLat
    #zmesh = f.variables[tmp[2]][:]
    if ("x" in tmp): 
        x = f.variables['x'][:] + eqLon
        y = f.variables['y'][:] + eqLat
    elif ("lat" in tmp): 
        x = f.variables['lon'][:] + eqLon
        y = f.variables['lat'][:] + eqLat

    zmesh = f.variables['z'][:]
    f.close()
    xmesh, ymesh = np.meshgrid(x, y)
    #print np.shape(zmesh), np.mean(zmesh)
    return xmesh, ymesh, zmesh


def sum_elements_displacement(idp, okDir, slipScale, slip, rake):
    """
    Sum of individual displacements for the selected sub faults 
    NOTE: Subduction Setting only
    """
  
    nSelSF = len(idp)
    zok = 0
    #fac0 = slip*np.cos(np.deg2rad(rake))/slipScale
    #fac90 = slip*np.sin(np.deg2rad(rake))/slipScale
    #fac0 = np.cos(np.deg2rad(rake))/slipScale
    #fac90 = np.sin(np.deg2rad(rake))/slipScale
    ic = 0
    for i in idp:
        filename0 = os.path.join(okDir+"0", str(i+1), "UZ_0_" + str(i+1) + ".nc")
        filename90 = os.path.join(okDir+"90", str(i+1), "UZ_90_" + str(i+1) + ".nc")
        xok, yok, zok0 = load_displacement_nc(filename0, 0., 0.)
        xok, yok, zok90 = load_displacement_nc(filename90, 0., 0.)
        ##zok += (zok0*fac0 + zok90*fac90)
        fac0 = np.cos(np.deg2rad(rake[i]))/slipScale
        fac90 = np.sin(np.deg2rad(rake[i]))/slipScale
        zok += slip[i]*(zok0*fac0 + zok90*fac90)
        ic += 1
  
    return xok, yok, zok


def main():
    print()


if __name__ == "__main__":
    main()
