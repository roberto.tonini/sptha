#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import numpy as np

def linear_combination(idSelG, zSelG, normF, nTS, hTS, ntstep):
    """
  
    """
  
    iblock = 40
    nblock = int(nTS/iblock)+1
    heightTS = np.zeros((ntstep,nTS))
    for i in range(nblock):
        istart = i*iblock
        if i == nblock-1:
            iend = nTS
        else:
            iend = istart + iblock
        hTS1 = hTS[idSelG,istart:iend,:]
        htmp = normF*hTS1*np.transpose(np.tile(zSelG, (ntstep,len(hTS1[0,:,0]),1)))
        heightTS[:,istart:iend] = np.transpose(np.sum(htmp, axis=0))

    return heightTS


def selection(xok, yok, zok, xbat, ybat, zbat, lonG, latG, isc):
    """
  
    """
  
    sigma = 4000.0
    height = 10.0
    cond1 = np.amin(xbat) < np.amin(xok)
    cond2 = np.amax(xbat) > np.amax(xok)
    cond3 = np.amin(ybat) < np.amin(yok)
    cond4 = np.amax(ybat) > np.amax(yok)
    if cond1 and cond2 and cond3 and cond4:
        xb_tmp = np.where( (xbat > np.amin(xok)) & (xbat < np.amax(xok)) )[0]
        yb_tmp = np.where( (ybat > np.amin(yok)) & (ybat < np.amax(yok)) )[0]
        bz = zbat[np.amin(yb_tmp):np.amax(yb_tmp),np.amin(xb_tmp):np.amax(xb_tmp)]
        ind_z = np.where(np.ravel(bz) < 0.)[0]
        zsea = np.ravel(zok)[ind_z]
        if len(zsea) < 2:
            return [0], np.array([0]), 0
        amp = np.amax(zsea)-np.amin(zsea)
    else:
        amp = np.amax(zok)-np.amin(zok)
  
    if (amp < 0.1):  
        return [0], np.array([0]), 0
    elif (amp >= 0.1 and amp <= 1.0):  
        okth = amp/50.
    else:
        okth = amp/25.
  
  
    #okth = amp/float(th_sel)
    tmp = np.where(np.abs(zok) > okth)
    idx = tmp[0]
    idy = tmp[1]
    xx_tmp = xok[idx,idy]
    yy_tmp = yok[idx,idy]
    zz_tmp = zok[idx,idy]
    xmin = np.amin(xx_tmp)
    xmax = np.amax(xx_tmp) 
    ymin = np.amin(yy_tmp)
    ymax = np.amax(yy_tmp)

    tmp1 = np.where( (lonG > xmin) & (lonG < xmax) )[0]
    tmp2 = np.where( (latG > ymin) & (latG < ymax) )[0]
    idG = np.intersect1d(tmp1, tmp2)
    nG = len(idG)
    if nG < 1:
        return [0], np.array([0]), 0
  
    xxx = xok.ravel()
    yyy = yok.ravel()
    zzz = zok.ravel()
    xok_tmp = np.where( (xxx > xmin) & (xxx < xmax) )[0]
    yok_tmp = np.where( (yyy > ymin) & (yyy < ymax) )[0]
    idok = np.intersect1d(xok_tmp, yok_tmp)
    xx = xxx[idok]
    yy = yyy[idok]
    zz = zzz[idok]
  
    xG = lonG[idG]
    yG = latG[idG]
    xminG = np.amin(xG)
    xmaxG = np.amax(xG) 
    yminG = np.amin(yG)
    ymaxG = np.amax(yG) 
    nxG = int(1+(xmaxG-xminG)/0.075)
    nyG = nG/nxG 
    #nyG = int(1+(ymaxG-yminG)/0.06667) 

    set1 = np.vstack((xx,yy)).transpose()
    set2 = np.vstack((xG,yG)).transpose()
  
    conv = 1852.*60.
    #d = distance.cdist(set2, set1, 'euclidean')*conv
    #zG = zz[np.argmin(d,axis=1)]
    #print np.shape(set1), np.shape(set2), np.shape(d), np.shape(zG)
    #eta = height*np.exp(-0.5*(d/sigma)**2)
 
    zG = np.zeros((nG))
    sumOk = np.zeros((len(xx)))
    for i in range(nG):
        d = np.sqrt(np.sum((set1 - set2[i,:])**2,axis=1))
        zG[i] = zz[np.argmin(d)]
        eta = height*np.exp(-0.5*(d*conv/sigma)**2)
        sumOk = sumOk + zG[i]*eta
        #sumOk = sumOk + zG[i]*eta[i,:]

    if ( (np.amax(sumOk)-np.amin(sumOk))/height > 0.01 ): 
        normGFactor = (np.amax(zok)-np.amin(zok))/(np.amax(sumOk)-np.amin(sumOk))
    else:
        normGFactor= 0

    print(isc+1, nG, normGFactor, np.amax(zok), np.amin(zok), np.amax(sumOk), np.amin(sumOk))
    return idG, np.array(zG), normGFactor


def load_centres(filename=None):
    """
    """
    if filename == None:
        filename = '/work/tonini/sptha/data/gaussians/gauss_grid_MED.txt'

    if (os.path.exists(filename)):
        tmp = np.loadtxt(filename, skiprows=0)
        lon = tmp[:,3] 
        lat = tmp[:,4]
        idg = tmp[:,0].astype(int)
        idLandSea = tmp[:,5].astype(int)
        ind = np.where(idLandSea > 0)[0]
        return lon, lat, idg, ind
    else:
        msg = "ERROR: file {0} does not exist."
        sys.exit(msg.format(filename))


def main():
    print()


if __name__ == "__main__":
    main()
