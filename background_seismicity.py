#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import numpy as np
from scipy.io import netcdf


def load_cells(filename=None):
    """
    Load spatial discretization (cells) for the background seismicity (BS) 
    """
    
    if filename == None:
        cru_dir = "/work/tonini/sptha/data/background_seismicity/"
        filename = os.path.join(cru_dir, "TopoCrustCoarseGrid100x80Full.txt")

    with open(filename, "r") as f:
        lon = []
        lat = []
        depth = []
        #f.readline() # uncomment only if filename has the first line header
        for line in f.readlines():
            tmp = line.split()
            lon.append(float(tmp[10]))
            lat.append(float(tmp[11]))
            depth.append(float(tmp[18]))

    return lon, lat, depth


def seismogenic_depth(n, w, seism_depth):
    """
    Calculation of depth ranges for crustal seismicity depending on 
    seismogenic boundary thickness and corresponding rates.
    n = n. of magnitudes
    w:  fault's width
    dsb: seismogenic boundary thickness
    d2: control depth for higher Mw
    dtop = top of the fault depth
    """
    
    dmin = 1.
    #r2 = 1./np.sqrt(2.)   # sin(45)
    r2 = 0.5               # sin(30)
    dd = 0.5*w*r2
    #print(dd)
    lim_inf = seism_depth - 2*dd
    nd = 1 + np.ceil((lim_inf)/dd).astype(int)
    #print(lim_inf, nd)
    #sys.exit()
    dtop = [[0.]]*n
    ind = np.where(nd <= 1)[0]
    
    for i in ind:
        if dmin+dd[i] <= seism_depth:
            dtop[i] = [dmin]
        else:
            dtop[i] = [0]

    ind = np.where(nd > 1)[0]
    for i in ind:
        tmp = np.arange(dmin,seism_depth,dd[i])
        #print(tmp, dd[i])
        if (nd[i] == 2 and tmp[1]-tmp[0] < dd[i]):
            dtop[i] = list(tmp[:-1])
        else:
            dtop[i] = list(tmp[:-1])

    return dtop



def main():
    print()


if __name__ == "__main__":
    main()
