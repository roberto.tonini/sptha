#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Library of functions used to calculate SPTHA

"""

import os
import sys
import numpy as np
from shapely.geometry import Point,Polygon,MultiPolygon


def calc_fault_verts(x, y, projection, length, width, strike, dip, rake):
    """
    Calculate the coordinates of the verteces of the fault
    """
    print(x, y, projection, length, width, strike, dip, rake)
    d = dip*np.pi/180.
    s = strike*np.pi/180.
  
    dl = 0.5*length
    dw = 0.5*width*np.cos(d)

    if (projection == "GEO"):
        dx = calc_geo_distance(x, y, x+1, y)
        dy = calc_geo_distance(x, y, x, y+1)
    elif (projection == "UTM"):
        dx = 1
        dy = 1
    else:
        sys.exit("ERROR: Unknown projection")  

    a = dl*np.sin(s)/dx
    b = dl*np.cos(s)/dy
    c = dw*np.sin(s)/dy
    d = dw*np.cos(s)/dx
  
    xv = np.array([x+a-d, x+a+d, x-a+d, x-a-d])
    yv = np.array([y+b+c, y+b-c, y-b-c, y-b+c])

    return xv, yv


def calc_geo_distance(lon1, lat1, lon2, lat2):
    """
    Calculate the distance in km between two points given in lon,lat coordinates. 
    Earth is approximated with a sphere of constant radius.
    Spherical law of cosines:
    d = acos( sin(lat1)*sin(lat2) + cos(lat1)*cos(lat2)*cos(lon2-lon1) )*R
    """

    earth_radius = 6371000
    deg2rad = np.pi/180.0
    lo1 = lon1*deg2rad
    lo2 = lon2*deg2rad
    la1 = lat1*deg2rad
    la2 = lat2*deg2rad
    arg = np.sin(la1)*np.sin(la2) + np.cos(la1)*np.cos(la2)*np.cos(lo2-lo1)
    d = earth_radius*np.arccos(arg)
    return d


def calc_rigidity(depth):
    """
    """
    a = 0.5631
    b = 0.0437
    mu = np.power(10, a + b*depth)*1.e9  
    ind = np.where(depth > 29.)
    mu[ind] = 70.*1.e9 
    return mu
  

def calc_slip(l, w, m0):
    """
    Calculate slip
  
    Input: 
      - m0 = Seismic Moment
      - l = Lenght
      - w = Width
    Output:
      - s = slip  
    """
  
    mu = 3.0e10   # 30 GPa
    s = m0/(mu*l*w*1e6)  
    return s


def create_fault(ds, lon, lat, proj, length, width, strike, dip, rake):
    """
    """
    x, y = calc_fault_verts(lon, lat, proj, (length+ds)*1000, 
                          (width+ds)*1000, strike, dip, rake)

    f = np.array([[x[0],y[0]], [x[1],y[1]], 
                  [x[2],y[2]], [x[3],y[3]], 
                  [x[0],y[0]]])

    p = Polygon(f)
    return x, y, p   
  

def load_angles(dirname, filename):
    """
    """

    f = os.path.join(dirname, filename)
    if (os.path.exists(f)):
        tmp = np.loadtxt(f, skiprows=0)
        s = tmp[:,0]
        d = tmp[:,1]
        r = tmp[:,2]
        # back from Selva and Marzocchi 2004
        ind = np.where(d > 90)
        s[ind] = s[ind] + 180 
        d[ind] = 180 - d[ind] 
    else:
        msg = "ERROR: Angles file input is missing."
        sys.exit(msg)  

    na = len(s)
    return s, d, r, na


def load_magnitude(f, scaling_law):
    """
    it loads magnitude discretization and the corresponding 
    fault size (length, width).
      bs = background seismicity
      ps = prevalent seismicity
    """
    if (os.path.exists(f)):
        tmp = np.loadtxt(f, skiprows=0)
        mw = tmp[:,0]
        if scaling_law == 'bs':
            length = tmp[:,1]
            width = tmp[:,2]
        elif scaling_law == 'ps':
            length = tmp[:,4]
            width = tmp[:,5]
        else:
            sys.exit('ERROR: Bad scaling law inserted')
    else:
        msg = "ERROR: Magnitude input file is missing."
        sys.exit(msg)

    return mw, length, width




#### FROM HERE, NOT REVISED


#def calcPdep(w, d1, d2):
#    """
#    Calculation of depth ranges for crustal seismicity depending on 
#    seismogenic boundary thickness and corresponding rates.
#    w:  fault's width
#    d1: seismogenic depth
#    d2: control depth for higher Mw
#    """
#  
#    minTop = 1.
#    depthSB = d1
#    r2 = 1./np.sqrt(2.)
#    dd = 0.5*w*r2
#    nDepth = 1 + np.ceil((depthSB-2*dd-1)/dd).astype(int)
#    if nDepth <= minTop:
#        pd = [1.]
#    if nDepth > minTop:
#        tmp = np.linspace(1,depthSB-2*dd,nDepth)
#        ptmp = [1/float(nDepth) for j in range(nDepth)]
#        if (nDepth == 2 and tmp[1]-tmp[0] < d2):
#            pd = [1.]
#        else:
#            pd = ptmp
#  
#    return pd


def define_regions(filename=None):
    """
    """
    if (filename==None):
        filename = "/work/tonini/sptha/data/geographic/TectonicRegions_20141027B.txt"

    # Defining regions' boundaries
    f = open(filename, "r")
    f.readline()
    f.readline()
    lonReg = []
    latReg = []
    nptReg = []
    indReg = []
    nameReg = []
    nRegions = 0
    for line in f.readlines():
        if (line[0] == ">"):
            tmp = line.split(",")
            indReg.append(int(tmp[0][1:]))
            nptReg.append(int(tmp[1]))
            nameReg.append(tmp[2])
            nRegions += 1 
        else:
            tmp = line.split()
            lonReg.append(float(tmp[1]))
            latReg.append(float(tmp[0]))
  
    # Defining regions' polygons
    ini = 0
    polygon_regions = [None for i in range(nRegions)]
    for i in range(nRegions):
        end = ini + nptReg[i]
        b = np.array([lonReg[ini:end], latReg[ini:end]]).transpose()
        polygon_regions[indReg[i]-1] = Polygon(tuple(b))
        #pRegion.append(Polygon(tuple(b)))
        ini = end
  
    return polygon_regions


def fps_map(filename=None):
    """
    """
    if filename==None:
        filename = "/scratch/projects/cat/SPTHA/data/commons/fcp/fcp_all_depth.txt"

    T = np.loadtxt(filename)
    lonTS = T[:,0]
    latTS = T[:,1]
    dTS = T[:,2]
    #nTS, ntmp = np.shape(T)
  
    ## mapping of TS with respect sub DBs (iTSDB) and global DB (iTS)
    #TSDB = np.loadtxt("/scratch/projects/cat/inputData/Italy_points_ZONE_index.txt")
    #iTSDB = TSDB[:,2].astype(int)
    #iTS = TSDB[:,3].astype(int)
    #nTStot, ntmp = np.shape(TSDB)
    
    #return iTSDB, iTS, nTStot, lonTS, latTS, dTS
    return lonTS, latTS, dTS
  

if __name__ == "__main__":
    print()


